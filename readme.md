
$ docker build -t microsoft/pyodbc3 .   
$ docker run -it microsoft/pyodbc3   
$ docker build -t sql-3 .   
## Crear contenedor con prueto
$ docker run -p 8888:8888 --name trimble-con trimble-img   
## Crear contenedor con puerto y volumen
$ docker run -p 8888:8888 -v /home/acrispin/code/python/ds/report_pandas_trimble:/app --name trimble-con trimble-img   
## Crear contenedor con puerto y volumen en windows
$ docker run -p 8888:8888 -v d\code\python\data-science\report_trimble\src:/app --name trimble-con trimble-img   
$ docker run -p 8888:8888 -v c\Users\acrispin\Documents\Kitematic\report_trimble\src:/app --name trimble-con trimble-img   

Driver pyodbc   
https://github.com/mkleehammer/pyodbc/wiki/Connecting-to-SQL-Server-from-Windows   

http://biobits.org/bokeh-flask.html   
https://bokeh.pydata.org/en/latest/docs/user_guide/server.html#single-module-format   
https://github.com/sandipde/bokeh-docker   
https://github.com/lukauskas/docker-bokeh/blob/master/docker/Dockerfile   
https://bokeh.pydata.org/en/latest/docs/user_guide/annotations.html   
https://bokeh.pydata.org/en/latest/docs/user_guide/layout.html   
https://pandas.pydata.org/pandas-docs/stable/merging.html   
https://bokeh.pydata.org/en/latest/docs/user_guide/categorical.html   
https://bokeh.pydata.org/en/latest/docs/user_guide/data.html#filtering-data-with-cdsview   
https://stackoverflow.com/questions/39401481/how-to-add-data-labels-to-a-bar-chart-in-bokeh   
  
Links utils  
* http://scientificpythonsnippets.com/index.php/distributions/5-read-clipboard-data-with-pandas
* https://stackoverflow.com/questions/41247345/python-read-cassandra-data-into-pandas
* 
* real time dashboard data analytics python pandas bokeh
* https://blog.sicara.com/bokeh-dash-best-dashboard-framework-python-shiny-alternative-c5b576375f7f
* https://towardsdatascience.com/data-visualization-with-bokeh-in-python-part-iii-a-complete-dashboard-dc6a86aa6e23
* 
* pandas scatter plot
* https://chrisalbon.com/python/data_visualization/matplotlib_scatterplot_from_pandas/
* 
* pandas scatter plot with color ranges
* https://programandociencia.com/2016/05/12/complex-scatter-plots-on-python-part-ii-defining-colors-labels-and-title/
* 
* update docker app
* https://bobcares.com/blog/update-docker-image/
* https://bobcares.com/blog/update-docker-image/2/
* 
* seaborn with flask
* http://biobits.org/bokeh-flask.html
* 
* bokeh server with port
* https://bokeh.pydata.org/en/latest/docs/user_guide/server.html#single-module-format
* https://github.com/sandipde/bokeh-docker/blob/master/Dockerfile
* https://github.com/lukauskas/docker-bokeh/blob/master/docker/Dockerfile
* http://alanpryorjr.com/2017-09-24-stockstreamer/
* 
* bokeh class server http_server_kwargs
* http://bokeh.pydata.org/en/latest/_modules/bokeh/server/server.html
* 
* lock object python
* https://hackernoon.com/synchronization-primitives-in-python-564f89fee732
