from datetime import datetime

# http://www.regisblog.fr/2017/12/09/log-into-a-file-with-bokeh/
import logging
from bokeh.util import logconfig

import pyodbc as pyodbc
from decouple import config, Csv
import matplotlib.pyplot as plt
import pandas as pd
import textwrap
from bokeh.plotting import figure
from bokeh.server.server import Server
import bokeh.models as bmo
from bokeh.layouts import row, column
from bokeh.models.widgets import Div

logconfig.basicConfig(level=logging.INFO, filename='the_log_file.log')  
logger = logging.getLogger('THE_LOGGER')


DB_SERVER = config('DB_SERVER', default='localhost')
DB_PORT = config('DB_PORT', default='1433')
DB_NAME = config('DB_NAME', default='dbname')
DB_USER = config('DB_USER', default='username')
DB_PASSWORD = config('DB_PASSWORD', default='password')

connStr = ('DRIVER={ODBC Driver 17 for SQL Server};Server=%s,%s;port=%s;Database=%s;uid=%s;pwd=%s;') \
         % (DB_SERVER, DB_PORT, DB_PORT, DB_NAME, DB_USER, DB_PASSWORD)

cnxn = pyodbc.connect(connStr)
# cursor = cnxn.cursor()

sql = textwrap.dedent("""
select sm.* ,
    CASE 
        WHEN sm.diff_media BETWEEN 0 AND 29.999999 THEN 1
        WHEN sm.diff_media BETWEEN 30 AND 59.999999 THEN 2
        WHEN sm.diff_media BETWEEN 60 AND 89.999999 THEN 3
        ELSE 4
    END AS media_scale,
    CONVERT(INT, 
        CASE 
            WHEN ISNUMERIC(SUBSTRING(sm.truck_code,1,1))=1 THEN sm.truck_code 
            ELSE SUBSTRING(sm.truck_code,2,LEN(sm.truck_code)-1) 
        END) 
    AS truck_code_num
from replcs.dbo.trim_trama_sum sm WITH(NOLOCK)
JOIN [ReplCS].[dbo].[tgps] WITH(NOLOCK) ON  sm.order_date=CONVERT(DATE,tgps.order_date) 
                                      --AND sm.truck_code=RTRIM(LTRIM(tgps.truck_code))
                                      AND sm.tkt_code=RTRIM(LTRIM(tgps.tkt_code))
                                      AND sm.order_code=RTRIM(LTRIM(tgps.order_code))
WHERE sm.order_date = '2018-05-16' -- CONVERT(DATE,GETDATE()) 
    and  sm.num_rec > 0 and fe_ini_receiver is not null
ORDER BY sm.order_date
    ,sm.truck_code
    ,sm.tkt_code
""")

TOOLS="crosshair,pan,wheel_zoom,zoom_in,zoom_out,box_zoom,undo,redo,reset,tap,save,box_select,poly_select,lasso_select,"

scale_colors = {
        1:'#00FF00',
        2:'#FFFF00',
        3:'#4169E1',
        4:'#FF0000'
    }

scale_legend = {
        1:'00 a 30 seg',
        2:'30 a 60 seg',
        3:'60 a 90 seg',
        4:'90 seg a más'
    }    

def get_count_scales(_df):
    gp = _df.groupby(['media_scale'])['truck_code'].count()
    gpf = gp.to_frame()
    total_camiones = gp.sum()
    dfTemp = pd.DataFrame({'truck_code': [0, 0, 0, 0]}, index=[1, 2, 3, 4])
    dfTemp.index.name = "media_scale"
    dfFinal = gpf.join(dfTemp, how='right', lsuffix='_left', rsuffix='_right')
    serieFinal = dfFinal['truck_code_left'].fillna(0)
    serieFinalPorcentajes = serieFinal / (total_camiones * 0.01)
    # return list(serieFinal), list(serieFinalPorcentajes.round(2))
    return list(serieFinal), [str(i) + '%' for i in serieFinalPorcentajes.round(2)]


def modify_doc(doc):
    logger.info('modify_doc ' + datetime.now().strftime("%Y-%m-%d %H:%M:%S.%f"))
    dfQ = pd.read_sql(sql, cnxn)
    ref_colors = [scale_colors[x] for x in dfQ['media_scale']]
    p = figure(tools=TOOLS, plot_width=1200, plot_height=800, title="Indicador de latencia")
    r = p.scatter(dfQ.truck_code_num, dfQ.diff_media, 
                  fill_color=ref_colors, fill_alpha=0.6, 
                  line_color=None, size=25)
    p.xaxis[0].axis_label = 'Camiones'
    p.yaxis[0].axis_label = 'Latencia (Segundos)'
    ds = r.data_source
    source = bmo.ColumnDataSource(dfQ)
    labels = bmo.LabelSet(x='truck_code_num', y='diff_media', text='truck_code', level='glyph',
              x_offset=3, y_offset=3, source=source, render_mode='canvas', text_font_size="9pt")
    p.add_layout(labels)

    scales = ['00 a 30 seg', '30 a 60 seg', '60 a 90 seg', '90 seg a más']
    countScales = get_count_scales(dfQ)
    colors = ['#00FF00','#FFFF00','#4169E1','#FF0000']
    source2 = bmo.ColumnDataSource(data=dict(scales=scales, counts=countScales[0], percents=countScales[1], colors=colors))
    p2 = figure(x_range=scales, plot_height=250, title="Escalas", toolbar_location=None, tools="")
    # p2.vbar(x=scales, top=counts, width=0.9, fill_color=colors)
    p2.vbar(x='scales', top='counts', width=0.9, fill_color='colors', source=source2)
    p2.xgrid.grid_line_color = None
    p2.y_range.start = 0
    p2.xaxis[0].axis_label = 'Escalas'
    p2.yaxis[0].axis_label = 'Número de camiones'
    labels2 = bmo.LabelSet(x='scales', y='counts', text='percents', level='glyph',
        x_offset=-25, y_offset=-25, source=source2, render_mode='canvas')
    p2.add_layout(labels2)
    
    def update():
        logger.info('update ' + datetime.now().strftime("%Y-%m-%d %H:%M:%S.%f"))
        dfQ = pd.read_sql(sql, cnxn)
        ref_colors = [scale_colors[x] for x in dfQ['media_scale']]
        new_data = dict()
        new_data['x'] = dfQ.truck_code_num
        new_data['y'] = dfQ.diff_media
        new_data['fill_color'] = ref_colors
        ds.data = new_data
        source.data = bmo.ColumnDataSource(data=dfQ).data
        countScales = get_count_scales(dfQ)
        source2.data['counts'] = countScales[0]
        source2.data['percents'] = countScales[1]

    doc.add_periodic_callback(update, 1000*1)

    layout = row(p, p2)
    doc.title = "Indicadores de latencia Trimble"
    doc.add_root(layout)


APP_SERVER = config('APP_SERVER', default='localhost')
APP_PORT = config('APP_PORT', default='5001')
ALLOWED_WS_ORIGIN = config('ALLOWED_WS_ORIGIN', default='localhost', cast=Csv())
ALLOWED_WS_ORIGIN = [i + ':' + APP_PORT for i in ALLOWED_WS_ORIGIN]

# server = Server({'/': modify_doc}, address='172.27.12.185', port=5001, allow_websocket_origin=['172.27.12.185:5001'])
server = Server({'/': modify_doc}, address=APP_SERVER, port=int(APP_PORT), allow_websocket_origin=ALLOWED_WS_ORIGIN)
server.start()


if __name__ == '__main__':
    logger.info('Opening Bokeh application on http://{0}:{1}/'.format(APP_SERVER, APP_PORT))
    server.io_loop.add_callback(server.show, "/")
    server.io_loop.start()